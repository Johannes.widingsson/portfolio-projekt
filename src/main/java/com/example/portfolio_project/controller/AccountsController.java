package com.example.portfolio_project.controller;

import com.example.portfolio_project.entities.Accounts;

import com.example.portfolio_project.service.AccountsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Objects;

@Controller
@SessionAttributes("accounts")
public class AccountsController {

    @Autowired
    private AccountsService accountsService;

    // Skickar registreringformuläret för kunden in i db
    @PostMapping("/accounts/register")
    public String registerNewAccounts(Accounts accounts){

        if(accounts.isCompanyCustomer() ){
            accounts.setRole("Företagskund");

        }else if (accounts.isPrivateCustomer()){
            accounts.setRole("Privatkund");
        }

        accountsService.register(accounts);

        return "redirect:/accounts";
    }

    //Visar registreringsidan, med hjälp av model kan vi få med oss
    // customer i vår hemsida så vi kan få tag på variablerna för att
    // senare lagra värdena genom de.
    @GetMapping("/accounts/create_account")
    public String viewHomePage(Model model){

        Accounts newAccount = new Accounts();
        //String customer = "Customer";

        //model.addAttribute("customerRole", customer);
        model.addAttribute("accounts", newAccount);

        return "registration";
    }

    //Hämtar kunderna från databasen läggs in i en lista som
    //sedan skrivs ut i webbsidan.
    @GetMapping("/accounts")
    public String showAccountsList(Model model){
        List<Accounts> listAccounts =  accountsService.listAll();
        model.addAttribute("listAccounts", listAccounts);

        return "accounts";
    }

    @GetMapping("/accounts/login")
    public String logIn( Model model){


        model.addAttribute("account", new Accounts());
        return "login";
    }

    @PostMapping("/account/login/log")
    public String logInVal(Accounts accounts, Model model,  RedirectAttributes redirectAttributes){
        Accounts auth = accountsService.validate(accounts.getEmail(),accounts.getPassword());

        if(Objects.nonNull(auth)){
            model.addAttribute("accounts", auth);

            //redirectAttributes.addAttribute("auth", auth);
            return "redirect:/booking";
        }
        else {
            return "redirect:/accounts/login";
        }
    }

    @GetMapping("/mina_sidor")
    public String viewCustomerPage(Accounts accounts, Model model){

        //Accounts currentAuth = auth;

        model.addAttribute("accounts", accounts);
        return "customer_page";
    }

    @GetMapping("/profil")
    public String viewProfile(){

        return "profile_customer";
    }
}
