package com.example.portfolio_project.controller;

import com.example.portfolio_project.entities.Accounts;
import com.example.portfolio_project.entities.Booking;

import com.example.portfolio_project.entities.Review;
import com.example.portfolio_project.service.AccountsService;
import com.example.portfolio_project.service.BookingService;

import com.example.portfolio_project.service.ReviewService;
import com.example.portfolio_project.service.UserNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.awt.print.Book;
import java.sql.Time;
import java.util.List;

@Controller
@SessionAttributes({"accounts", "cleaners"})
public class BookingController {


    @Autowired private BookingService bookingService;
    @Autowired private AccountsService accountsService;

    @Autowired private ReviewService reviewService;




    //Hämtar bokningsidan, med hjälp av model kan vi få med oss
    // booking i vår hemsida så vi kan få tag på variablerna för att
    //senare lagra värdena genom de.
    @GetMapping("/booking")
    public String viewBooking( Model model, Accounts accounts) throws UserNotFoundException {

        model.addAttribute("accounts", accounts);
        List<Booking> listBookings = bookingService.listAllBooking();

        model.addAttribute("listBookings", listBookings);
        model.addAttribute("booking", new Booking());
        return "booking";
    }

    //Post(Skickar), lägger till bokade städningar in i vår db
    @PostMapping("/booking/add")
    public String addBooking(Booking booking, Accounts accounts, Model model){


        booking.setAccounts(accounts);

        bookingService.addBooking(booking);
        return "redirect:/booking";
    }

    @GetMapping("booking/delete/{bookingId}")
    public String deleteBooking(@PathVariable("bookingId") Integer id){

        bookingService.deleteBooking(id);

        return "redirect:/booking";
    }


    @PostMapping("/booking/update")
    public String editThisBooking(Booking booking, Accounts accounts){

        booking.setAccounts(accounts);

        bookingService.editBooking(booking);

        return "redirect:/booking";
    }

    @GetMapping("booking/edit/{bookingId}")
    public String editBooking(@PathVariable("bookingId") Integer id, Model model, RedirectAttributes ra, Accounts accounts)  {

        List<Booking> listBookings = bookingService.listAllBooking();

        try{
            Booking booking = bookingService.get(id);
            model.addAttribute("booking", booking);
            model.addAttribute("accounts", accounts);
            model.addAttribute("listBookings", listBookings);
            return "edit_booking";

        }catch (UserNotFoundException e){
            ra.addAttribute("message",  "The user has been saved succesfully ");
            return "redirect:/booking";
        }

    }


    @GetMapping("/booking/review")
    public String viewReview(Model model, Accounts accounts, Booking booking){
        //Byt ut denna lista med genomförda bokningar som städaren har gjort
        model.addAttribute("accounts", accounts);
        model.addAttribute("bookings", booking);

        List<Booking> listBookings = bookingService.listAllBooking();
        model.addAttribute("listBookings", listBookings);
        model.addAttribute("review", new Review());

        return "review" ;
    }

    @PostMapping(value = "/booking/review/post", params = "app")
    public String postApproval(Review review, Accounts accounts, Booking booking, Model model, @RequestParam(name="bookingId") Integer bookingId) throws UserNotFoundException {


        //Booking booking1 = bookingService.get(bookingId);

        //review.setBooking(booking1);

        List<Booking> listBookings = bookingService.listAllBooking();
        for(int i = 0;  i < listBookings.size(); i++){
            if(listBookings.get(i).getBookingId() == bookingId)
            review.setBooking(listBookings.get(i));
            review.setSelection(listBookings.get(i).getSelection());
            review.setTime(listBookings.get(i).getTime());
            review.setDate(listBookings.get(i).getDate());

        }


        review.setApproval(true);
        review.setDisapproval(false);
        review.setAccounts(accounts);

        //Läggs in i tabellen för Reviews
        reviewService.addReview(review);
        return "redirect:/tomt";

        //return "redirect:/booking";
    }

    @PostMapping(value = "/booking/review/post", params = "disapp")
    public String postDisapproval( Booking booking, Review review, Accounts accounts, @RequestParam(name="bookingId") Integer bookingId) throws UserNotFoundException {

        List<Booking> listBookings = bookingService.listAllBooking();
        for(int i = 0;  i < listBookings.size(); i++){
            if(listBookings.get(i).getBookingId() == bookingId)
                review.setBooking(listBookings.get(i));
            review.setSelection(listBookings.get(i).getSelection());
            review.setTime(listBookings.get(i).getTime());
            review.setDate(listBookings.get(i).getDate());

        }
        review.setApproval(false);
        review.setDisapproval(true);

        review.setAccounts(accounts);
        //review.setTime(listBookings.get().);


        //Läggs in i tabellen för Reviews
        reviewService.addReview(review);
        return "redirect:/tomt";
    }


    @GetMapping("/tomt")
    public String viewReviews(){
        return "tomt";
    }


}
