package com.example.portfolio_project.repository;

import com.example.portfolio_project.entities.Booking;

import org.springframework.data.repository.CrudRepository;

public interface BookingRepository extends CrudRepository<Booking, Integer> {

}
