package com.example.portfolio_project.entities;

import javax.persistence.*;

@Entity
@Table(name ="booking_histories")
public class BookingHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer bookingHistoryId;

    @Column(length = 45)
    private String date;

    @Column(length = 45)
    private String time;

    @Column(length = 45)
    private String selection;

    @ManyToOne
    @JoinColumn(name ="bookingId")
    private Booking booking;

}
