package com.example.portfolio_project.entities;


import javax.persistence.*;

@Entity
@Table(name = "bookings")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer bookingId;

    @Column(length = 45)
    private String date;

    @Column(length = 45)
    private String time;

    @Column(length = 45)
    private String selection;

    @ManyToOne
    @JoinColumn(name = "accid")
    private Accounts accounts;

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }

    public Accounts getAccounts() {
        return accounts;
    }

    public void setAccounts(Accounts accounts) {
        this.accounts = accounts;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "bookingId=" + bookingId +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", selection='" + selection + '\'' +
                ", accounts=" + accounts +
                '}';
    }
}
